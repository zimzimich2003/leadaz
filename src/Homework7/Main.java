package Homework7;

import java.util.Random;

public class Main {
    public static void main(String[] args) {

        Human[] human = new Human[createLengthArrayOfPeople()];

        for(int i = 0; i < human.length; i++) {                      // ���������� ������� ��������� 'human'
            human[i] = new Human("Human" + i, "HumanSurname" + i, createRandomAge());
            System.out.println(human[i]);
        }

        boolean isSorted = false;                                    // ���������� ������ 'human' �� ����������� ���� 'age'
        Human buffer;
        while (!isSorted) {
            isSorted = true;
            for(int i = 0; i <human.length -1; i++) {
                if(human[i].getAge() > human[i+1].getAge()) {
                    isSorted = false;
                    buffer = human[i];
                    human[i] = human[i+1];
                    human[i+1] = buffer;
                }
            }
        }

        System.out.println("��������������� ������");               //����� ���������������� ������ 'human'

        for(int i = 0; i < human.length; i++) {
            System.out.println(human[i]);
        }

    }

    public static int createLengthArrayOfPeople() {               // ����� ������� ��������� ���������� ����� � �������
        Random random = new Random();
        int arrayForRandomLength = random.nextInt(10 +1);
        System.out.println("������ ������ �� " + arrayForRandomLength + " �������");
        return arrayForRandomLength;
    }

    public static int createRandomAge(){                  //����� ������ ��������� �������� ��� ���� 'age' ������� Human
        Random random = new Random();
        int randomAge = random.nextInt(99 +1);
        return randomAge;
    }


}



