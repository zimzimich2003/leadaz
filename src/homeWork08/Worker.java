package homeWork08;

public class Worker {
    private String name;
    private String lastName;
    private String profession;

    public Worker() {}

    public Worker(String name, String lastName, String profession) {
        this.name = name;
        this.lastName = lastName;
        this.profession = profession;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getProfession() {
        return profession;
    }

    public void goToWork(){                                    // ����� � ������� ������� ������
        System.out.println("\n****�������� ������������ �����!****");
        System.out.println("��� ��������!!!");
    }

    public void goToVacation(int days) {                       // ����� � ������� ������ ������
        System.out.println("������� ���� " + days + " ��������� ����. �������� ��� 365 ����!!!!");
    }

}
