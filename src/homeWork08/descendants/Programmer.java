package homeWork08.descendants;

import homeWork08.Worker;

public class Programmer extends Worker {
    public Programmer(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }

    @Override
    public void goToWork() {                                    // ����� � ������� ������� ������
        System.out.println("\n****�������� ����� �������****");
        System.out.println("" + getName() +" " + getLastName() + " - ���������: " + getProfession());
    }

    @Override
    public void goToVacation(int days) {                         // ����� � ������� ������ ������
        System.out.println(getName() + " " + getLastName() + "  - ������ � ������ �� " + days + " ���� c 10 �������");;
    }

}
