package homeWork08;

import homeWork08.descendants.Devops;
import homeWork08.descendants.Programmer;
import homeWork08.descendants.SysAdmin;
import homeWork08.descendants.Tester;

public class Main {
    public static void main(String[] args) {

        Worker[] workersArray = new Worker[5];
        workersArray[0] = new Worker();
        workersArray[1] = new Programmer("������", "����", "�����������");
        workersArray[2] = new SysAdmin("������", "����", "SysAdmin");
        workersArray[3] = new Devops("�������", "�����", "DevOps");
        workersArray[4] = new Tester("������", " ��������", "Tester");

        int vacationDay = 30;                                //����� ��������� ����

        for (int i = 0; i < workersArray.length; i++) {
            workersArray[i].goToWork();                      //����� ������ ������
            workersArray[i].goToVacation(vacationDay);       // ����� ������ �������
            vacationDay-=5;                                  // ���������� ������� �� 5 ���� ��� ����������� �������
        }

    }
}
