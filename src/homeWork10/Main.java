package homeWork10;

public class Main {
    public static void main(String[] args) {
        Sequence sequence = new Sequence();

        int[] array = new int[10];                                       // �������� ������� �� 10 ���������
        System.out.println("�������������� ������:");
        for (int i = 0; i < array.length; i++) {                         // ���������� ������� ���������� ���������� �� 0 �� 640 ������������
            array[i] = (int) (Math.random() * 640 + 1);
            System.out.print(array[i] + " ");
        }

        System.out.println("\n--------------------------------------");

        System.out.println("׸���� �����:");

        sequence.filter(array, number -> number % 2 == 0);                 // ����� �� ��������

        System.out.println("��������� �� ����� ���� ����� �������:");

        sequence.filter(array, number -> checkingForEvenSumDigits(number));  //����� �� ����������� �������� ����� �����

        System.out.println("�������� ���� ���� �����:");

        sequence.filter(array, number -> checkingAllEvenNumber(number));     // ����� �� �������� ���� ���� �����

        System.out.println("������������� �����:");
        sequence.filter(array, number -> palindromeNumber(number));         // ������ �� �������������


    }

    private static boolean checkingForEvenSumDigits(int number) {       // ����� �� 2 ������
        int sum = 0;
        int bufferOfNumber = number;

        for (; bufferOfNumber > 0; bufferOfNumber /= 10) {           // �������� ����� �����
            sum += bufferOfNumber % 10;
        }

        if (sum % 2 == 0) {                                        // ��������� �� �������� ���������� �����
            return true;
        }
        return false;

    }

    private static boolean checkingAllEvenNumber(int number) {    // ����� � 3 �����
        int bufferOfNumber = number;

        for (; bufferOfNumber > 0; bufferOfNumber /= 10) {        // ����������� �������� ���� �����
            if (bufferOfNumber % 2 != 0) {
                return false;
            }
        }
        return true;
    }

    private static boolean palindromeNumber(int number) {         // ����� � 4 ������
        int reversedNumber = 0;
        int bufferOfNumber = number;
        int digit;

        while (bufferOfNumber != 0) {
            digit = bufferOfNumber % 10;
            reversedNumber = reversedNumber * 10 + digit;
            bufferOfNumber /= 10;
        }
        return ((number == reversedNumber) ? true : false);
    }

}
