package homeWork05;

public class Task02 {
    public static void main(String[] args) {
        int[] initialArray = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        sortMass(initialArray);
    }

    public static void sortMass(int[] initialArray) {
        int countPlus = 0;
        int countMinus = initialArray.length-1;
        int[] sortArray = new int[initialArray.length];
        for(int i = 0; i < initialArray.length; i++) {
            if(initialArray[i] > 0) {
                sortArray[countPlus] = initialArray[i];
                countPlus++;
            }else {
                sortArray[countMinus] = initialArray[i];
                countMinus--;

            }
        }
        for(int j = 0; j < sortArray.length; j++) {
            System.out.print(sortArray[j] + ", ");
        }
    }

}
